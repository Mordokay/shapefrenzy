﻿using UnityEngine;
using System.Collections;

public class flameThrowerItem : MonoBehaviour {

    GameManager gm;
    float flameThrowerTime;

    void Start()
    {
        flameThrowerTime = 10.0f;
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            gm.flameThrowerTime = this.flameThrowerTime;
            gm.flameThrowerActivated = true;

            gm.activateFlame();
            Destroy(this.gameObject);
        }
        else if (coll.gameObject.tag.Equals("Bomb")) {
            coll.GetComponent<BombManager>().igniteBomb();
        }
    }
}
