﻿using UnityEngine;
using System.Collections;

public class SuperStarMode : MonoBehaviour {

    GameObject myPlayer;
    GameManager gm;
    float superStarTime;

    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        superStarTime = 10.0f;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            myPlayer.GetComponent<PlayerMovement>().superStarMode = true;
            myPlayer.GetComponent<PlayerMovement>().superStarTime = superStarTime;

            gm.activateSuperStar();
            Destroy(this.gameObject);
        }
    }
}
