﻿using UnityEngine;
using System.Collections;

public class bubbleController : MonoBehaviour {

    GameObject myPlayer;
    float shieldTime;
    GameManager gm;

    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        shieldTime =10.0f;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            myPlayer.GetComponent<PlayerMovement>().isShielded = true;
            myPlayer.GetComponent<PlayerMovement>().shieldTime = shieldTime;

            gm.activateBubble();

            Destroy(this.gameObject);
        }
    }
}
