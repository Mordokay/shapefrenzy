﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class GameManager : MonoBehaviour {

    public bool isPaused;
    public GameObject player;

    public GameObject toggleMute;
    public GameObject toggleFreejoystickMode;
    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject helpMenu;
    public Button continueButton;
    public Slider volumeSlider;
    public Slider difficultySlider;
    public Text difficultyValue;
    public Slider itemRatioSlider;
    public Text itemRatioValue;

    public GameObject controlsText;
    public GameObject RedBallinfo;
    public GameObject BlueBallInfo;
    public GameObject YellowBallInfo;

    public float lastVolume;

    public GameObject heart1;
    public GameObject heart2;
    public GameObject heart3;
    public GameObject heart4;
    public GameObject heart5;

    public Text scoreText;
    public Text highScoreText;

    public int numberOfPoints;
    public int numberOfLives;
    public int highScore;
    public int combo;

    //thisvalue controlls the combo value
    public int comboValue;
    //this value controls the scale of the bar
    public float comboBarValue;
    //value of combo loss over time
    float comboDecreaseValue = 0.05f;

    public BarController barController;
    public SpawnManager spawnmanager;

    public bool hourGlassActivated;
    public float hourGlassTime;

    public bool isTripleChainning;
    public float tripleChainTime;

    float maxDifficulty;
    float maxItemRatio;
    float difficultyIncreasePointsRatio;

    //other variables
    GameObject[] blueBalls;
    GameObject[] redBalls;
    GameObject[] yellowBalls;

    public bool flameThrowerActivated;
    public float flameThrowerTime;

    void Start()
    {
        //garbage Collector every New Game
        System.GC.Collect();

        maxDifficulty = 100.0f;
        maxItemRatio = 100000.0f;
        difficultyIncreasePointsRatio = 10000.0f;
        isTripleChainning = true;
        comboValue = 1;
        comboBarValue = 0.0f;
        barController = this.GetComponent<BarController>();
        barController.barDisplay = comboBarValue;
        barController.barText = "Combo level " + comboValue;

        if (Time.time == 0)
        {
            Time.timeScale = 0.0f;
            isPaused = true;
            Cursor.visible = true;
            showHelpMenu();
            continueButton.interactable = false;
        }
        lastVolume = 50.0f;
        isPaused = false;

        numberOfPoints = 0;
        numberOfLives = 3;
        combo = 0;
        highScore = PlayerPrefs.GetInt("highscore");


        spawnmanager.difficulty = ((int)(PlayerPrefs.GetFloat("Difficulty") * maxDifficulty));
        difficultyValue.text = ((int)(PlayerPrefs.GetFloat("Difficulty") * maxDifficulty)).ToString();
        difficultySlider.value = PlayerPrefs.GetFloat("Difficulty");

        this.GetComponent<ItemSpawner>().frequencyDrop = ((int)(PlayerPrefs.GetFloat("itemRatio") * maxItemRatio));
        itemRatioValue.text = ((int)(PlayerPrefs.GetFloat("itemRatio") * maxItemRatio)).ToString();
        itemRatioSlider.value = PlayerPrefs.GetFloat("itemRatio");

        if (PlayerPrefs.GetInt("FreeJoystick") == 1)
        {
            this.GetComponent<TouchManager>().freeTouchMovement = true;
            toggleFreejoystickMode.GetComponent<Toggle>().isOn = true;

        }
        else {
            this.GetComponent<TouchManager>().freeTouchMovement = false;
            toggleFreejoystickMode.GetComponent<Toggle>().isOn = false;
        }
    }

    public void resetHighScore()
    {
        PlayerPrefs.SetInt("highscore", 0);
        this.highScore = 0;
    }

    public void showMenu()
    {
        mainMenu.SetActive(true);
        continueButton.interactable = true;
        optionsMenu.SetActive(false);
        helpMenu.SetActive(false);
    }

    public void showHelpMenu()
    {
        mainMenu.SetActive(true);
        continueButton.interactable = true;
        optionsMenu.SetActive(false);
        helpMenu.SetActive(true);
        showInstructions();
    }

    public void hideMenu()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(false);
        helpMenu.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void NewGame()
    {
        Time.timeScale = 1.0f;
        isPaused = false;
        SceneManager.LoadScene(0);
    }

    public void showInstructions()
    {
        mainMenu.SetActive(false);
        helpMenu.SetActive(true);
        showControlsInfo();
    }

    public void OptionsMenu()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }

    public void returnToMainMenu()
    {
        mainMenu.SetActive(true);
        optionsMenu.SetActive(false);
        helpMenu.SetActive(false);
    }

    public void returnToGame()
    {
        Time.timeScale = 1.0f;
        isPaused = false;
        Cursor.visible = false;
        hideMenu();
    }

    public void CheckMute()
    {
        if (toggleMute.GetComponent<Toggle>().isOn)
        {
            AudioListener.pause = true;
            lastVolume = volumeSlider.value;
            volumeSlider.value = 0;
        }
        else
        {
            AudioListener.pause = false;
            volumeSlider.value = lastVolume;
        }
    }

    public void CheckFreeJoystick()
    {
        if (toggleFreejoystickMode.GetComponent<Toggle>().isOn)
        {
            PlayerPrefs.SetInt("FreeJoystick", 1);
            this.GetComponent<TouchManager>().freeTouchMovement = true;
        }
        else
        {
            PlayerPrefs.SetInt("FreeJoystick", 0);
            this.GetComponent<TouchManager>().freeTouchMovement = false;
        }
    }

    public void CheckSoundVolume()
    {
        AudioListener.volume = volumeSlider.value;
        if (volumeSlider.value == 0)
        {
            toggleMute.GetComponent<Toggle>().isOn = true;
        }
        else
        {
            toggleMute.GetComponent<Toggle>().isOn = false;
        }
    }

    public void CheckDifficulty()
    {
        if(Time.timeSinceLevelLoad != 0) { 
            difficultyValue.text = ((int)(difficultySlider.value * maxDifficulty)).ToString();
            spawnmanager.difficulty = (int)(difficultySlider.value * maxDifficulty);
            PlayerPrefs.SetFloat("Difficulty", difficultySlider.value);
        }
    }

    public void CheckItemRatio()
    {
        if (Time.timeSinceLevelLoad != 0)
        {
            itemRatioValue.text = ((int)(itemRatioSlider.value * maxItemRatio)).ToString();
            this.GetComponent<ItemSpawner>().frequencyDrop = (int)(itemRatioSlider.value * maxItemRatio);
            PlayerPrefs.SetFloat("itemRatio", itemRatioSlider.value);
        }
    }

    public void showControlsInfo()
    {
        controlsText.SetActive(true);
        RedBallinfo.SetActive(false);
        BlueBallInfo.SetActive(false);
        YellowBallInfo.SetActive(false);
    }
    public void showRedBallinfo()
    {
        controlsText.SetActive(false);
        RedBallinfo.SetActive(true);
        BlueBallInfo.SetActive(false);
        YellowBallInfo.SetActive(false);
    }
    public void showBlueBallInfo()
    {
        controlsText.SetActive(false);
        RedBallinfo.SetActive(false);
        BlueBallInfo.SetActive(true);
        YellowBallInfo.SetActive(false);
    }
    public void showYellowBallInfo()
    {
        controlsText.SetActive(false);
        RedBallinfo.SetActive(false);
        BlueBallInfo.SetActive(false);
        YellowBallInfo.SetActive(true);
    }

    public void handleUIMenuButton() {
        if (isPaused)
        {
            Time.timeScale = 1.0f;
            isPaused = false;
            hideMenu();
        }
        else
        {
            Time.timeScale = 0.0f;
            isPaused = true;
            continueButton.interactable = true;
            showMenu();
        }
    }
    public void releaseBalls() {
        blueBalls = GameObject.FindGameObjectsWithTag("blueBall");
        redBalls = GameObject.FindGameObjectsWithTag("redBall");
        yellowBalls = GameObject.FindGameObjectsWithTag("yellowBall");

        foreach (GameObject blue in blueBalls)
        {
            if (blue.GetComponent<BlueBallManager>().lastVelocity.Equals(Vector3.zero))
                blue.GetComponent<BlueBallManager>().lastVelocity = new Vector3(Random.Range(-20.0f, 20.0f), Random.Range(-20.0f, 20.0f), 0.0f);
            blue.GetComponent<Rigidbody2D>().velocity = blue.GetComponent<BlueBallManager>().lastVelocity;
            blue.GetComponent<BlueBallManager>().velocityLock = false;
        }
        foreach (GameObject red in redBalls)
        {
            if (red.GetComponent<RedBallManager>().lastVelocity.Equals(Vector3.zero))
                red.GetComponent<RedBallManager>().lastVelocity = new Vector3(Random.Range(-20.0f, 20.0f), Random.Range(-20.0f, 20.0f), 0.0f);
            red.GetComponent<Rigidbody2D>().velocity = red.GetComponent<RedBallManager>().lastVelocity;
            red.GetComponent<RedBallManager>().velocityLock = false;
        }
        foreach (GameObject yellow in yellowBalls)
        {
            if (yellow.GetComponent<YellowBallManager>().lastVelocity.Equals(Vector3.zero))
                yellow.GetComponent<YellowBallManager>().lastVelocity = new Vector3(Random.Range(-20.0f, 20.0f), Random.Range(-20.0f, 20.0f), 0.0f);
            yellow.GetComponent<Rigidbody2D>().velocity = yellow.GetComponent<YellowBallManager>().lastVelocity;
            yellow.GetComponent<YellowBallManager>().velocityLock = false;
        }
    }

    public void activateBubble() {
        isTripleChainning = false;
        tripleChainTime = 0.0f;
        flameThrowerActivated = false;
        flameThrowerTime = 0.0f;
        player.GetComponent<PlayerMovement>().disableSuperStarMode();

        player.GetComponent<PlayerMovement>().shotgunAmmo = 0;
        player.GetComponent<PlayerMovement>().shotgunActive = false;
    }

    public void activateFlame()
    {
        isTripleChainning = false;
        tripleChainTime = 0.0f;
        player.GetComponent<PlayerMovement>().disableSuperStarMode();
        player.GetComponent<PlayerMovement>().disableShield();

        player.GetComponent<PlayerMovement>().shotgunAmmo = 0;
        player.GetComponent<PlayerMovement>().shotgunActive = false;
    }

    public void activateTriple() {
        player.GetComponent<PlayerMovement>().disableSuperStarMode();
        player.GetComponent<PlayerMovement>().disableShield();
        flameThrowerActivated = false;
        flameThrowerTime = 0.0f;

        player.GetComponent<PlayerMovement>().shotgunAmmo = 0;
        player.GetComponent<PlayerMovement>().shotgunActive = false;
    }

    public void activateSuperStar()
    {
        player.GetComponent<PlayerMovement>().disableShield();
        flameThrowerActivated = false;
        flameThrowerTime = 0.0f;
        isTripleChainning = false;
        tripleChainTime = 0.0f;

        player.GetComponent<PlayerMovement>().shotgunAmmo = 0;
        player.GetComponent<PlayerMovement>().shotgunActive = false;
    }

    public void activateShotgun()
    {
        player.GetComponent<PlayerMovement>().disableSuperStarMode();
        player.GetComponent<PlayerMovement>().disableShield();
        flameThrowerActivated = false;
        flameThrowerTime = 0.0f;
        isTripleChainning = false;
        tripleChainTime = 0.0f;
    }

    void Update()
    {

        if ((numberOfPoints / difficultyIncreasePointsRatio) > (PlayerPrefs.GetFloat("Difficulty") * maxDifficulty)){
            PlayerPrefs.SetFloat("Difficulty", (numberOfPoints / difficultyIncreasePointsRatio) / maxDifficulty);
            difficultySlider.value = PlayerPrefs.GetFloat("Difficulty");
            difficultyValue.text = ((int)(difficultySlider.value * maxDifficulty)).ToString();
            spawnmanager.difficulty = (int)(difficultySlider.value * maxDifficulty);
            PlayerPrefs.SetFloat("Difficulty", difficultySlider.value);
        }

        //Combo updater
        if (comboBarValue < 0)
        {
            if (comboValue > 1)
            {
                comboValue -= 1;
                comboBarValue = 1.0f;
                barController.barText = "Combo level " + comboValue;
            }
        }
        else if (comboBarValue > 1.0f)
        {
            comboValue += 1;
            comboBarValue = 0.5f;
            barController.barText = "Combo level " + comboValue;
        }
        else {
            comboBarValue -= (Time.deltaTime * comboDecreaseValue * (comboValue / 5.0f));
            Mathf.Clamp(comboBarValue, 0.0f, 1.0f);
            this.GetComponent<BarController>().barDisplay = comboBarValue;
        }

        //HourGlass updater
        if (hourGlassActivated)
        {
            blueBalls = GameObject.FindGameObjectsWithTag("blueBall");
            redBalls = GameObject.FindGameObjectsWithTag("redBall");
            yellowBalls = GameObject.FindGameObjectsWithTag("yellowBall");

            foreach (GameObject blue in blueBalls)
            {
                if (!blue.GetComponent<BlueBallManager>().velocityLock) {
                    blue.GetComponent<BlueBallManager>().lastVelocity = blue.GetComponent<Rigidbody2D>().velocity;
                    blue.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                    blue.GetComponent<BlueBallManager>().velocityLock = true;
                }
            }
            foreach (GameObject red in redBalls)
            {
                if (!red.GetComponent<RedBallManager>().velocityLock)
                {
                    red.GetComponent<RedBallManager>().lastVelocity = red.GetComponent<Rigidbody2D>().velocity;
                    red.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                    red.GetComponent<RedBallManager>().velocityLock = true;
                }
            }
            foreach (GameObject yellow in yellowBalls)
            {
                if (!yellow.GetComponent<YellowBallManager>().velocityLock)
                {
                    yellow.GetComponent<YellowBallManager>().lastVelocity = yellow.GetComponent<Rigidbody2D>().velocity;
                    yellow.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                    yellow.GetComponent<YellowBallManager>().velocityLock = true;
                }
            }
            hourGlassTime -= Time.deltaTime;
            if (hourGlassTime < 0) {
                hourGlassActivated = false;
                releaseBalls();
            }
        }

        //TripleChainning updater
        if (isTripleChainning) {
            tripleChainTime -= Time.deltaTime;
            if(tripleChainTime < 0) {
                isTripleChainning = false;
                this.GetComponent<ChainManager>().isChainning = false;
                this.GetComponent<ChainManager>().isChainning2 = false;
                this.GetComponent<ChainManager>().isChainning3 = false;
                releaseBalls();
            }
        }

        //Flame Thrower updater
        if (flameThrowerActivated) {
            flameThrowerTime -= Time.deltaTime;
            if (flameThrowerTime < 0)
            {

                flameThrowerActivated = false;
            }
        }

        if (numberOfPoints > highScore)
        {
            highScore = numberOfPoints;
            PlayerPrefs.SetInt("highscore", highScore);
        }

        scoreText.text = "SCORE: " + numberOfPoints;
        highScoreText.text = "HIGHSCORE: " + highScore.ToString();

        //Number of lives Controller
        switch (numberOfLives)
        {
            case 0:
                mainMenu.SetActive(true);
                isPaused = true;
                continueButton.interactable = false;
                heart1.SetActive(false);
                heart2.SetActive(false);
                heart3.SetActive(false);
                heart4.SetActive(false);
                heart5.SetActive(false);
                break;
            case 1:
                heart1.SetActive(true);
                heart2.SetActive(false);
                heart3.SetActive(false);
                heart4.SetActive(false);
                heart5.SetActive(false);
                break;
            case 2:
                heart1.SetActive(true);
                heart2.SetActive(true);
                heart3.SetActive(false);
                heart4.SetActive(false);
                heart5.SetActive(false);
                break;
            case 3:
                heart1.SetActive(true);
                heart2.SetActive(true);
                heart3.SetActive(true);
                heart4.SetActive(false);
                heart5.SetActive(false);
                break;
            case 4:
                heart1.SetActive(true);
                heart2.SetActive(true);
                heart3.SetActive(true);
                heart4.SetActive(true);
                heart5.SetActive(false);
                break;
            case 5:
                heart1.SetActive(true);
                heart2.SetActive(true);
                heart3.SetActive(true);
                heart4.SetActive(true);
                heart5.SetActive(true);
                break;
            default:
                break;
        }
    }
}
