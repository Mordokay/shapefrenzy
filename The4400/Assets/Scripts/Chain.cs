﻿using UnityEngine;
using System.Collections;

public class Chain : MonoBehaviour {

    ChainManager chainManager;
    
    void Start () {
        chainManager = GameObject.Find("GameManager").GetComponent<ChainManager>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "redBall" || coll.gameObject.tag == "Borders" || coll.gameObject.tag == "blueBall" || coll.gameObject.tag == "yellowBall")
        {
            chainManager.breakChain = true;
            chainManager.touchedChain = this.gameObject;
        }
    }
}
