﻿using UnityEngine;
using System.Collections;

public class flameThrower : MonoBehaviour {

    void OnParticleCollision(GameObject other)
    {
        if (other.tag.Equals("blueBall")) {
            other.GetComponent<BlueBallManager>().breakBall();
        }
        else if (other.tag.Equals("redBall"))
        {
            other.GetComponent<RedBallManager>().breakBall();
        }
        else if (other.tag.Equals("yellowBall"))
        {
            other.GetComponent<YellowBallManager>().breakBall();
        }
        else if (other.tag.Equals("Bomb"))
        {
            other.GetComponent<BombManager>().igniteBomb();
        }
    }
}
