﻿using UnityEngine;
using System.Collections;

public class TripleChain : MonoBehaviour {

    GameManager gm;
    float tripleChainTime;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        tripleChainTime = 10.0f;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            gm.isTripleChainning = true;
            gm.tripleChainTime = tripleChainTime;
            gm.activateTriple();
            Destroy(this.gameObject);
        }
    }
}
