﻿using UnityEngine;
using System.Collections;

public class ShotgunController : MonoBehaviour {

    GameObject myPlayer;
    GameManager gm;
    int shotgunAmmo;

    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        shotgunAmmo = 10;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            gm.activateShotgun();
            myPlayer.GetComponent<PlayerMovement>().shotgunActive = true;
            myPlayer.GetComponent<PlayerMovement>().shotgunAmmo = shotgunAmmo;
            
            Destroy(this.gameObject);
        }
    }
}