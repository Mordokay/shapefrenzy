﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {


    public int difficulty;
    //Other Variables
    GameObject[] blueBalls;
    GameObject[] redBalls;
    GameObject[] yellowBalls;
    GameObject[] bombs;
    int ballValue;
    float redMinRange;
    float redMaxRange;
    float yellowMinRange;
    float yellowMaxRange;
    float blueMinRange;
    float blueMaxRange;
    float bombMinRange;
    float bombMaxRange;

    void Start() {
        redMinRange = 0;
        redMaxRange = 29;
        yellowMinRange = 30;
        yellowMaxRange = 59;
        blueMinRange = 60;
        blueMaxRange = 89;
        bombMinRange = 90;
        //bombMinRange = 20;
        bombMaxRange = 100;
    }

    void FixedUpdate() {

        blueBalls = GameObject.FindGameObjectsWithTag("blueBall");
        redBalls = GameObject.FindGameObjectsWithTag("redBall");
        yellowBalls = GameObject.FindGameObjectsWithTag("yellowBall");
        bombs = GameObject.FindGameObjectsWithTag("Bomb");

        ballValue = 0;
        foreach(GameObject blue in blueBalls) {
            ballValue += blue.GetComponent<BlueBallManager>().pointValue;
        }
        foreach (GameObject red in redBalls)
        {
            ballValue += red.GetComponent<RedBallManager>().pointValue;
        }
        foreach (GameObject yellow in yellowBalls)
        {
            ballValue += yellow.GetComponent<YellowBallManager>().pointValue;
        }

        foreach (GameObject bomb in bombs)
        {
            ballValue += bomb.GetComponent<BombManager>().pointValue;
        }

        if (ballValue < difficulty)
        {
            Vector3 direction = Vector3.up;
            float angle = transform.eulerAngles.z * Mathf.Deg2Rad;
            float sin = Mathf.Sin(angle);
            float cos = Mathf.Cos(angle);
            Vector3 forward = new Vector3(
                   direction.x * cos - direction.y * sin,
                   direction.x * sin + direction.y * cos,
                   0f);


            int random = Random.Range(0, 100);
            if (random >= redMinRange && random <= redMaxRange) {
                Object newBallBall = Instantiate(Resources.Load("RedBall", typeof(GameObject)), this.GetComponent<Transform>().position, Quaternion.identity);
                GameObject newBallAux = (GameObject)newBallBall;
                newBallAux.GetComponent<Rigidbody2D>().velocity = forward * 10.0f;
            }
            if (random >= yellowMinRange && random <= yellowMaxRange) {
                Object newBallBall = Instantiate(Resources.Load("YellowBall", typeof(GameObject)), this.GetComponent<Transform>().position, Quaternion.identity);
                GameObject newBallAux = (GameObject)newBallBall;
                newBallAux.GetComponent<Rigidbody2D>().velocity = forward * 10.0f;
            }
            if (random >= blueMinRange && random <= blueMaxRange) {
                Object newBallBall = Instantiate(Resources.Load("BlueBall", typeof(GameObject)), this.GetComponent<Transform>().position, Quaternion.identity);
                GameObject newBallAux = (GameObject)newBallBall;
                newBallAux.GetComponent<Rigidbody2D>().velocity = forward * 10.0f;
            }
            if (random >= bombMinRange && random <= bombMaxRange)
            {
                Object newBallBall = Instantiate(Resources.Load("Bomb", typeof(GameObject)), this.GetComponent<Transform>().position, Quaternion.identity);
                GameObject newBallAux = (GameObject)newBallBall;
                newBallAux.GetComponent<Rigidbody2D>().velocity = forward * 10.0f;
            }
        }
	}
}
