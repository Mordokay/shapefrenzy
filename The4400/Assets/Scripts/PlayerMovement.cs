﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour {

    public float maximumSpeed;
    public float speedMovement;
    GameObject gameManager;
    public ChainManager chainManager;

    public Joystick joystick;
    public Joystick joystickChain;

    public Vector3 movementTouchpad;
    public Vector3 bulletTouchpad;
    public Vector3 leftTouch;
    public Vector3 rightTouch;

    public bool isImune;
    public float imunityTime;
    public float imuneTimeAux;

    Vector3 normalizedmovement;
    public TouchManager touchManager;

    Rigidbody2D rb;
    Camera cam;

    public bool isShielded;
    public float shieldTime;
    public GameObject shield;

    public int shotgunAmmo;
    public bool shotgunActive;
    public float lastShotgunTime;
    public float shotgunIntervalBetweenShoots;
    public float shotgunShrapAngle;

    public bool superStarMode;
    public float superStarTime;
    public GameObject starParticles;
    Color playerColor;
    public Color superStarColor;
    public float speedBullet;

    public GameObject flameThrowerParticles;

    //Other variables
    GameObject[] blueBalls;
    GameObject[] redBalls;
    GameObject[] yellowBalls;
    float brakeSpeed;  // calculate the speed decrease
    Vector3 normalisedVelocity;
    Vector3 brakeVelocity;  // make the brake Vector3 value
    float speed;

    // Use this for initialization
    void Start () {
        shotgunShrapAngle = 5.0f;
        shotgunIntervalBetweenShoots = 0.5f;
        lastShotgunTime = shotgunIntervalBetweenShoots;
        speedBullet = 12.0f;
        playerColor = this.GetComponent<SpriteRenderer>().color;
        isImune = false;
        imunityTime = 2.0f;
        rb = GetComponent<Rigidbody2D>();
        gameManager = GameObject.Find("GameManager");
        cam = Camera.main;
        maximumSpeed = 6.0f;
        speedMovement = 6.0f;
        imuneTimeAux = imunityTime;
    }

    public void disableSuperStarMode() {
        superStarMode = false;
        starParticles.SetActive(false);
        superStarTime = 0.0f;
    }

    public void disableShield() {
        Color color = shield.GetComponent<SpriteRenderer>().color;
        color.a = 255;
        shield.GetComponent<SpriteRenderer>().color = color;
        this.GetComponent<SpriteRenderer>().color = playerColor;
        isShielded = false;
        shield.SetActive(false);
    }

    void ignoreBalls(bool value) {
        blueBalls = GameObject.FindGameObjectsWithTag("blueBall");
        redBalls = GameObject.FindGameObjectsWithTag("redBall");
        yellowBalls = GameObject.FindGameObjectsWithTag("yellowBall");

        foreach (GameObject blue in blueBalls)
        {
            Physics2D.IgnoreCollision(blue.GetComponent<Collider2D>(), this.GetComponent<Collider2D>(), value);
        }
        foreach (GameObject red in redBalls)
        {
            Physics2D.IgnoreCollision(red.GetComponent<Collider2D>(), this.GetComponent<Collider2D>(), value);
        }
        foreach (GameObject yellow in yellowBalls)
        {
            Physics2D.IgnoreCollision(yellow.GetComponent<Collider2D>(), this.GetComponent<Collider2D>(), value);
        }
    }

        // Update is called once per frame
        void Update () {
        if (isImune)
        {
            imuneTimeAux -= Time.deltaTime;
            if (imuneTimeAux < 0.0f)
            {
                imuneTimeAux = imunityTime;
                isImune = false;
            }
            if ((((int)(imuneTimeAux * 10.0f)) % 2) == 0)
                this.GetComponent<SpriteRenderer>().color = new Color(0, 200, 0, 0);
            else
            {
                this.GetComponent<SpriteRenderer>().color = new Color(0, 200, 0, 255);
            }
            ignoreBalls(true);
        }
        else if (superStarMode) {
            starParticles.SetActive(true);
            superStarTime -= Time.deltaTime;
            if (superStarTime < 0)
            {
                superStarMode = false;
                starParticles.SetActive(false);
            }
            Color color = superStarColor;
            if (superStarTime < 2 && superStarTime > 0)
            {
                if ((((int)(superStarTime * 10.0f)) % 2) == 0)
                {
                    color.a = 0;
                    this.GetComponent<SpriteRenderer>().color = color;
                }
                else
                {
                    color.a = 255;
                    this.GetComponent<SpriteRenderer>().color = color;
                }
            }
            else
            {
                color.a = 255;
                this.GetComponent<SpriteRenderer>().color = color;
            }
        }
        else if (isShielded)
        {
            shield.SetActive(true);
            shieldTime -= Time.deltaTime;

            Color color = shield.GetComponent<SpriteRenderer>().color;
            if (shieldTime < 2 && shieldTime > 0)
            {
                if ((((int)(shieldTime * 10.0f)) % 2) == 0)
                {
                    color.a = 0;
                    shield.GetComponent<SpriteRenderer>().color = color;
                }
                else
                {
                    color.a = 255;
                    shield.GetComponent<SpriteRenderer>().color = color;
                }
            }
            else
            {
                color.a = 255;
                shield.GetComponent<SpriteRenderer>().color = color;
                this.GetComponent<SpriteRenderer>().color = playerColor;
            }

            if (shieldTime < 0)
            {
                isShielded = false;
                shield.SetActive(false);
            }
        }
        else
        {
            this.GetComponent<SpriteRenderer>().color = playerColor;
            ignoreBalls(false);
        }

        //Camera follow player to a fixed range so camera does not go "out" of the map
        cam.transform.position = new Vector3 (Mathf.Clamp( this.transform.position.x, -5.0f, 5.0f), Mathf.Clamp(this.transform.position.y, -2.0f, 2.0f), cam.transform.position.z) ;

        float AngleRad;
        float angle;
        if (!gameManager.GetComponent<GameManager>().isPaused)
        {

            ///left Joystick
            if (joystick.m_VerticalVirtualAxis.GetValue + joystick.m_HorizontalVirtualAxis.GetValue != 0)
            {
                AngleRad = Mathf.Atan2(joystick.m_VerticalVirtualAxis.GetValue, joystick.m_HorizontalVirtualAxis.GetValue);
                angle = (180 / Mathf.PI) * AngleRad;
                rb.rotation = angle - 90;
                this.GetComponent<Rigidbody2D>().velocity =
                    new Vector2(joystick.m_HorizontalVirtualAxis.GetValue, joystick.m_VerticalVirtualAxis.GetValue) * speedMovement;
            }

            ///Right Joystick
            if (joystickChain.m_VerticalVirtualAxis.GetValue + joystickChain.m_HorizontalVirtualAxis.GetValue != 0)
            {
                if (gameManager.GetComponent<GameManager>().flameThrowerActivated)
                {
                    AngleRad = Mathf.Atan2(joystickChain.m_VerticalVirtualAxis.GetValue, joystickChain.m_HorizontalVirtualAxis.GetValue);
                    angle = (180 / Mathf.PI) * AngleRad;
                    rb.rotation = angle - 90;

                    flameThrowerParticles.SetActive(true);
                }
                else if (shotgunActive) {
                    lastShotgunTime += Time.deltaTime;

                    if (lastShotgunTime > shotgunIntervalBetweenShoots)
                    {
                        if (new Vector3(joystickChain.m_HorizontalVirtualAxis.GetValue, joystickChain.m_VerticalVirtualAxis.GetValue, 0.0f).magnitude > 0.5)
                        {
                            AngleRad = Mathf.Atan2(joystickChain.m_VerticalVirtualAxis.GetValue, joystickChain.m_HorizontalVirtualAxis.GetValue);
                            angle = (180 / Mathf.PI) * AngleRad;
                            rb.rotation = angle - 90;

                            for (int i = -2; i <= 2; i++)
                            {
                                Vector3 newDirectionWithShift = new Vector3((float)Mathf.Cos((angle + (i * shotgunShrapAngle)) * (Mathf.PI / 180)), (float)Mathf.Sin((angle + (i * shotgunShrapAngle)) * (Mathf.PI / 180)));
                                GameObject bullet = Instantiate(Resources.Load("Bullet"), this.transform.position + (new Vector3(joystickChain.m_HorizontalVirtualAxis.GetValue, joystickChain.m_VerticalVirtualAxis.GetValue, 0.0f).normalized * 0.5f), Quaternion.identity) as GameObject;
                                bullet.GetComponent<Rigidbody2D>().velocity = newDirectionWithShift.normalized * speedBullet;
                                bullet.GetComponent<Rigidbody2D>().rotation = angle + (i * shotgunShrapAngle);
                            }

                            lastShotgunTime = 0.0f;
                            shotgunAmmo -= 1;
                        }
                    }
                    if (shotgunAmmo < 0) {
                        shotgunAmmo = 0;
                        shotgunActive = false;
                        lastShotgunTime = 0.0f;
                    }
                }
                else
                {
                    if (chainManager.fingerReleased == true)
                    {
                        chainManager.breakChain = true;
                        chainManager.fingerReleased = false;
                    }

                    AngleRad = Mathf.Atan2(joystickChain.m_VerticalVirtualAxis.GetValue, joystickChain.m_HorizontalVirtualAxis.GetValue);
                    angle = (180 / Mathf.PI) * AngleRad;
                    rb.rotation = angle - 90;
                    if (new Vector3(joystickChain.m_HorizontalVirtualAxis.GetValue, joystickChain.m_VerticalVirtualAxis.GetValue, 0.0f).magnitude > 0.5)
                    {
                        if (!chainManager.isChainning)
                        {
                            chainManager.isChainning = true;
                            if (gameManager.GetComponent<GameManager>().isTripleChainning)
                            {
                                chainManager.isChainning2 = true;
                                chainManager.isChainning3 = true;
                            }
                            chainManager.chainStartPos = this.transform.position + (new Vector3(joystickChain.m_HorizontalVirtualAxis.GetValue, joystickChain.m_VerticalVirtualAxis.GetValue, 0.0f).normalized * 0.5f);
                            chainManager.chainDirection = new Vector3(joystickChain.m_HorizontalVirtualAxis.GetValue, joystickChain.m_VerticalVirtualAxis.GetValue, 0.0f);
                        }
                    }
                }
            }
            else if (joystickChain.m_VerticalVirtualAxis.GetValue + joystickChain.m_HorizontalVirtualAxis.GetValue == 0)
            {
                chainManager.fingerReleased = true;
                flameThrowerParticles.SetActive(false);
            }

            speed = Vector3.Magnitude(rb.velocity);  // test current object speed
            if (speed > maximumSpeed)
            {
                brakeSpeed = speed - maximumSpeed;  // calculate the speed decrease

                normalisedVelocity = rb.velocity.normalized;
                brakeVelocity = normalisedVelocity * brakeSpeed;  // make the brake Vector3 value

                rb.AddForce(-brakeVelocity);  // apply opposing brake force
            }
        }
    }
}
