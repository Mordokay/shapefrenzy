﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "redBall" || coll.gameObject.tag == "Borders" || coll.gameObject.tag == "blueBall" || coll.gameObject.tag == "yellowBall")
        {
            Destroy(this.gameObject);
        }
    }

}
