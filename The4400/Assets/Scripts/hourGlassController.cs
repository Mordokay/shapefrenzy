﻿using UnityEngine;
using System.Collections;

public class hourGlassController : MonoBehaviour {

    GameManager gm;
    float hourGlassTime;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        hourGlassTime = 5.0f;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            gm.hourGlassActivated = true;
            gm.hourGlassTime = hourGlassTime;
            Destroy(this.gameObject);
        }
    }
}
