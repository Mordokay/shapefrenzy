﻿using UnityEngine;
using System.Collections;

public class HeartController : MonoBehaviour
{
    GameManager gm;
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if(gm.numberOfLives < 5)
                gm.numberOfLives += 1;

            Destroy(this.gameObject);
        }
    }
}
